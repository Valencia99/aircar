CREATE TABLE "vehiculo"
(
    id_vehiculo serial NOT NULL,
    marca character varying(100) NOT NULL,
    modelo character varying(100) NOT NULL,
    aniu integer NOT NULL,
    color character varying(100) NOT NULL,
    placa character varying(100) NOT NULL,
    descripcion character varying(200) NOT NULL,
    estado_vehiculo boolean NOT NULL,
    fk_usuario integer,
    PRIMARY KEY (id_vehiculo)
);
ALTER TABLE "vehiculo"
    OWNER to postgres;


 CREATE TABLE "usuario"
(
    id_usuario serial NOT NULL,
    identificacion integer NOT NULL,
    nombre_completo character varying(100) NOT NULL,
    telefono integer NOT NULL,
    direccion character varying(100) NOT NULL,
    email character varying(100) NOT NULL,
    clave character varying(50) NOT NULL,
    rol character varying(50) NOT NULL,
    estado boolean NOT NULL,
    PRIMARY KEY (id_usuario)
);

ALTER TABLE "usuario"
    OWNER to postgres;


CREATE TABLE "reservacion"
(
    id_reservacion serial NOT NULL,
    fecha_entrega date NOT NULL,
    fecha_devolucion date NOT NULL,
    valor_seguro integer NOT NULL,
    valor_total integer NOT NULL,
    fk_id_oferta integer,
    numero_licencia integer NOT NULL,
    estado boolean NOT NULL,
    PRIMARY KEY (id_reservacion)
);

ALTER TABLE "reservacion"
    OWNER to postgres;


CREATE TABLE "oferta"
(
    id_oferta serial NOT NULL,
    fk_id_vehiculo integer,
    fk_id_usuario integer,
    fecha date NOT NULL,
    descripcion character varying(200) NOT NULL,
    valor_dia integer  NOT NULL,
    ubicacion character varying(50) NOT NULL,
    estado boolean NOT NULL,
    PRIMARY KEY (id_oferta)
);

ALTER TABLE "oferta"
    OWNER to postgres;
