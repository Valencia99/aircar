package com.proyecto.AirCar.dto;

import java.io.Serializable;
import java.util.Date;

public class VehiculoOfertaDTO implements Serializable {
    private Date fechaOferta;
    private String descripconOferta;
    private Integer valorDiaOferta;
    private String ubicacionOferta;
    private String marcaVehiculo;
    private String modeloVehiculo;
    private Integer aniuVehiculo;
    private String colorVehiculo;
    private String placaVehiculo;

    public VehiculoOfertaDTO() {
        //constructor
    }

    public VehiculoOfertaDTO(Date fechaOferta, String descripconOferta, Integer valorDiaOferta, String ubicacionOferta, String marcaVehiculo, String modeloVehiculo, Integer aniuVehiculo, String colorVehiculo, String placaVehiculo) {
        this.fechaOferta = fechaOferta;
        this.descripconOferta = descripconOferta;
        this.valorDiaOferta = valorDiaOferta;
        this.ubicacionOferta = ubicacionOferta;
        this.marcaVehiculo = marcaVehiculo;
        this.modeloVehiculo = modeloVehiculo;
        this.aniuVehiculo = aniuVehiculo;
        this.colorVehiculo = colorVehiculo;
        this.placaVehiculo = placaVehiculo;
    }

    public Date getFechaOferta() {
        return fechaOferta;
    }

    public void setFechaOferta(Date fechaOferta) {
        this.fechaOferta = fechaOferta;
    }

    public String getDescripconOferta() {
        return descripconOferta;
    }

    public void setDescripconOferta(String descripconOferta) {
        this.descripconOferta = descripconOferta;
    }

    public Integer getValorDiaOferta() {
        return valorDiaOferta;
    }

    public void setValorDiaOferta(Integer valorDiaOferta) {
        this.valorDiaOferta = valorDiaOferta;
    }

    public String getUbicacionOferta() {
        return ubicacionOferta;
    }

    public void setUbicacionOferta(String ubicacionOferta) {
        this.ubicacionOferta = ubicacionOferta;
    }

    public String getMarcaVehiculo() {
        return marcaVehiculo;
    }

    public void setMarcaVehiculo(String marcaVehiculo) {
        this.marcaVehiculo = marcaVehiculo;
    }

    public String getModeloVehiculo() {
        return modeloVehiculo;
    }

    public void setModeloVehiculo(String modeloVehiculo) {
        this.modeloVehiculo = modeloVehiculo;
    }

    public Integer getAniuVehiculo() {
        return aniuVehiculo;
    }

    public void setAniuVehiculo(Integer aniuVehiculo) {
        this.aniuVehiculo = aniuVehiculo;
    }

    public String getColorVehiculo() {
        return colorVehiculo;
    }

    public void setColorVehiculo(String colorVehiculo) {
        this.colorVehiculo = colorVehiculo;
    }

    public String getPlacaVehiculo() {
        return placaVehiculo;
    }

    public void setPlacaVehiculo(String placaVehiculo) {
        this.placaVehiculo = placaVehiculo;
    }
}