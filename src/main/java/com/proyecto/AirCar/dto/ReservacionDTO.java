package com.proyecto.AirCar.dto;

import javax.persistence.Column;
import java.util.Date;

public class ReservacionDTO {

    @Column(name = "fecha_entrega")
    private Date fechaEntrega;

    @Column(name = "fecha devolucion")
    private Date fechaDevolucion;

    @Column(name = "valor_seguro")
    private Integer valorSeguro;

    @Column(name = "valor_total")
    private Integer valorTotal;

    @Column(name = "fk_id_oferta")
    private Integer fkIdOferta;

    @Column(name = "numero_licencia")
    private Integer numeroLicencia;

    @Column(name = "estado")
    private Boolean estado;

    public ReservacionDTO() {
        //constructor
    }

    public ReservacionDTO(ReservacionDTO reservacionDTO) {
        this.fechaEntrega = reservacionDTO.fechaEntrega;
        this.fechaDevolucion = reservacionDTO.fechaDevolucion;
        this.valorSeguro = reservacionDTO.valorSeguro;
        this.valorTotal = reservacionDTO.valorTotal;
        this.fkIdOferta = reservacionDTO.fkIdOferta;
        this.numeroLicencia = reservacionDTO.numeroLicencia;
        this.estado = reservacionDTO.estado;
    }

    public Date getFechaEntrega() {
        return fechaEntrega;
    }

    public void setFechaEntrega(Date fechaEntrega) {
        this.fechaEntrega = fechaEntrega;
    }

    public Date getFechaDevolucion() {
        return fechaDevolucion;
    }

    public void setFechaDevolucion(Date fechaDevolucion) {
        this.fechaDevolucion = fechaDevolucion;
    }

    public Integer getValorSeguro() {
        return valorSeguro;
    }

    public void setValorSeguro(Integer valorSeguro) {
        this.valorSeguro = valorSeguro;
    }

    public Integer getValorTotal() {
        return valorTotal;
    }

    public void setValorTotal(Integer valorTotal) {
        this.valorTotal = valorTotal;
    }

    public Integer getFkIdOferta() {
        return fkIdOferta;
    }

    public void setFkIdOferta(Integer fkIdOferta) {
        this.fkIdOferta = fkIdOferta;
    }

    public Integer getNumeroLicencia() {
        return numeroLicencia;
    }

    public void setNumeroLicencia(Integer numeroLicencia) {
        this.numeroLicencia = numeroLicencia;
    }

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }
}
