package com.proyecto.AirCar.dto;

import com.proyecto.AirCar.model.Oferta;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.util.Date;

public class OfertaDTO {

    @ManyToOne
    @JoinColumn(name = "fk_id_vehiculo")
    private Oferta oferta;

    @Column(name = "fk_id_usuario")
    private Long fkIdUsuario;

    @Column(name = "fecha")
    private Date fecha;

    @Column(name = "descripcion")
    private String descripcon;

    @Column(name = "valor_dia")
    private Integer valorDia;

    @Column(name = "ubicacion")
    private String ubicacion;

    @Column(name = "estado")
    private Boolean estado;

    public OfertaDTO() {
        //constructor
    }

    public OfertaDTO(OfertaDTO ofertaDTO) {
        this.oferta = ofertaDTO.oferta;
        this.fkIdUsuario = ofertaDTO.fkIdUsuario;
        this.fecha = ofertaDTO.fecha;
        this.descripcon = ofertaDTO.descripcon;
        this.valorDia = ofertaDTO.valorDia;
        this.ubicacion = ofertaDTO.ubicacion;
        this.estado = ofertaDTO.estado;
    }

    public Oferta getOferta() {
        return oferta;
    }

    public void setOferta(Oferta oferta) {
        this.oferta = oferta;
    }

    public Long getFkIdUsuario() {
        return fkIdUsuario;
    }

    public void setFkIdUsuario(Long fkIdUsuario) {
        this.fkIdUsuario = fkIdUsuario;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getDescripcon() {
        return descripcon;
    }

    public void setDescripcon(String descripcon) {
        this.descripcon = descripcon;
    }

    public Integer getValorDia() {
        return valorDia;
    }

    public void setValorDia(Integer valorDia) {
        this.valorDia = valorDia;
    }

    public String getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }
}

