package com.proyecto.AirCar.dto;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

public class UsuarioDTO {

    @Column(name = "identificacion")
    private Integer identificacion;

    @Column(name = "nombre_completo")
    private String nombre_completo;

    @Column(name = "telefono")
    private Integer telefono;

    @Column(name = "direccion")
    private String direccion;

    @Column(name = "email")
    private String email;

    @Column(name = "clave")
    private String clave;

    @Column(name = "rol")
    private String rol;

    @Column(name = "estado")
    private Boolean estado;

    public UsuarioDTO() {

    }

    public UsuarioDTO(UsuarioDTO usuarioDTO) {
        this.identificacion = usuarioDTO.identificacion;
        this.nombre_completo = usuarioDTO.nombre_completo;
        this.telefono = usuarioDTO.telefono;
        this.direccion = usuarioDTO.direccion;
        this.email = usuarioDTO.email;
        this.clave = usuarioDTO.clave;
        this.rol = usuarioDTO.rol;
        this.estado = usuarioDTO.estado;
    }

    public Integer getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(Integer identificacion) {
        this.identificacion = identificacion;
    }

    public String getNombre_completo() {
        return nombre_completo;
    }

    public void setNombre_completo(String nombre_completo) {
        this.nombre_completo = nombre_completo;
    }

    public Integer getTelefono() {
        return telefono;
    }

    public void setTelefono(Integer telefono) {
        this.telefono = telefono;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }
}
