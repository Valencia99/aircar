package com.proyecto.AirCar.dto;

import javax.persistence.Column;

public class VehiculoDTO {

    @Column(name = "marca")
    private String marca;

    @Column(name = "modelo")
    private String modelo;

    @Column(name = "aniu")
    private Integer aniu;

    @Column(name = "color")
    private String color;

    @Column(name = "placa")
    private String placa;

    @Column(name = "descripcion")
    private String descripcion;

    @Column(name = "estado_vehiculo")
    private Boolean estadoVehiculo;

    @Column(name = "fk_usuario")
    private Integer usuarioVehiculo;

    public VehiculoDTO() {
    }

    public VehiculoDTO(VehiculoDTO vehiculoDTO) {
        this.marca = vehiculoDTO.marca;
        this.modelo = vehiculoDTO.modelo;
        this.aniu = vehiculoDTO.aniu;
        this.color = vehiculoDTO.color;
        this.placa = vehiculoDTO.placa;
        this.descripcion = vehiculoDTO.descripcion;
        this.estadoVehiculo = vehiculoDTO.estadoVehiculo;
        this.usuarioVehiculo = vehiculoDTO.usuarioVehiculo;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public Integer getAniu() {
        return aniu;
    }

    public void setAniu(Integer aniu) {
        this.aniu = aniu;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Boolean getEstadoVehiculo() {
        return estadoVehiculo;
    }

    public void setEstadoVehiculo(Boolean estadoVehiculo) {
        this.estadoVehiculo = estadoVehiculo;
    }

    public Integer getUsuarioVehiculo() {
        return usuarioVehiculo;
    }

    public void setUsuarioVehiculo(Integer usuarioVehiculo) {
        this.usuarioVehiculo = usuarioVehiculo;
    }
}
