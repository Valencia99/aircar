package com.proyecto.AirCar.converter;

import com.proyecto.AirCar.dto.VehiculoDTO;
import com.proyecto.AirCar.model.Vehiculo;

public class VehiculoConverter extends AbstractConverter<Vehiculo, VehiculoDTO> {

    public VehiculoConverter() {
        //constructor
    }

    @Override
    public Vehiculo fromDto(VehiculoDTO dto) {
        Vehiculo vehiculo = new Vehiculo();
        vehiculo.setAniu(dto.getAniu());
        vehiculo.setColor(dto.getColor());
        vehiculo.setDescripcion(dto.getDescripcion());
        vehiculo.setEstadoVehiculo(dto.getEstadoVehiculo());
        vehiculo.setMarca(dto.getMarca());
        vehiculo.setModelo(dto.getModelo());
        vehiculo.setPlaca(dto.getPlaca());
        vehiculo.setUsuarioVehiculo(dto.getUsuarioVehiculo());

        return vehiculo;
    }

    @Override
    public VehiculoDTO fromEntity(Vehiculo entity) {
        VehiculoDTO vehiculoDTO = new VehiculoDTO();
        vehiculoDTO.setAniu(entity.getAniu());
        vehiculoDTO.setColor(entity.getColor());
        vehiculoDTO.setDescripcion(entity.getDescripcion());
        vehiculoDTO.setEstadoVehiculo(entity.getEstadoVehiculo());
        vehiculoDTO.setMarca(entity.getMarca());
        vehiculoDTO.setModelo(entity.getModelo());
        vehiculoDTO.setPlaca(entity.getPlaca());
        vehiculoDTO.setUsuarioVehiculo(entity.getUsuarioVehiculo());

        return vehiculoDTO;
    }
}
