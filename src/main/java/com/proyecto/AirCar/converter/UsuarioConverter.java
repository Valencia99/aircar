package com.proyecto.AirCar.converter;

import com.proyecto.AirCar.dto.UsuarioDTO;
import com.proyecto.AirCar.model.Usuario;

public class UsuarioConverter extends AbstractConverter<Usuario, UsuarioDTO>{
    @Override
    public Usuario fromDto(UsuarioDTO dto) {
        Usuario usuario = new Usuario();
        usuario.setIdentificacion(dto.getIdentificacion());
        usuario.setNombre_completo(dto.getNombre_completo());
        usuario.setTelefono(dto.getTelefono());
        usuario.setDireccion(dto.getDireccion());
        usuario.setEmail(dto.getEmail());
        usuario.setClave(dto.getClave());
        usuario.setRol(dto.getRol());
        usuario.setEstado(dto.getEstado());
        return usuario;
    }

    @Override
    public UsuarioDTO fromEntity(Usuario entity) {
        UsuarioDTO usuarioDTO = new UsuarioDTO();
        usuarioDTO.setIdentificacion(entity.getIdentificacion());
        usuarioDTO.setNombre_completo(entity.getNombre_completo());
        usuarioDTO.setTelefono(entity.getTelefono());
        usuarioDTO.setDireccion(entity.getDireccion());
        usuarioDTO.setEmail(entity.getEmail());
        usuarioDTO.setClave(entity.getClave());
        usuarioDTO.setRol(entity.getRol());
        usuarioDTO.setEstado(entity.getEstado());

        return usuarioDTO;
    }
}
