package com.proyecto.AirCar.converter;

import com.proyecto.AirCar.dto.ReservacionDTO;
import com.proyecto.AirCar.model.Reservacion;

public class ReservacionConverter extends AbstractConverter<Reservacion, ReservacionDTO> {

    public ReservacionConverter() {
        //constructor
    }

    @Override
    public Reservacion fromDto(ReservacionDTO dto) {
        Reservacion reservacion = new Reservacion();
        reservacion.setEstado(dto.getEstado());
        reservacion.setFechaDevolucion(dto.getFechaDevolucion());
        reservacion.setFechaEntrega(dto.getFechaEntrega());
        reservacion.setFkIdOferta(dto.getFkIdOferta());
        reservacion.setNumeroLicencia(dto.getNumeroLicencia());
        reservacion.setValorSeguro(dto.getValorSeguro());
        reservacion.setValorTotal(dto.getValorTotal());

        return reservacion;
    }

    @Override
    public ReservacionDTO fromEntity(Reservacion entity) {
        ReservacionDTO reservacionDTO = new ReservacionDTO();
        reservacionDTO.setEstado(entity.getEstado());
        reservacionDTO.setFechaDevolucion(entity.getFechaDevolucion());
        reservacionDTO.setFechaEntrega(entity.getFechaEntrega());
        reservacionDTO.setFkIdOferta(entity.getFkIdOferta());
        reservacionDTO.setNumeroLicencia(entity.getNumeroLicencia());
        reservacionDTO.setValorSeguro(entity.getValorSeguro());
        reservacionDTO.setValorTotal(entity.getValorTotal());

        return reservacionDTO;
    }

}
