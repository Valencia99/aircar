package com.proyecto.AirCar.converter;

import com.proyecto.AirCar.dto.OfertaDTO;
import com.proyecto.AirCar.model.Oferta;

public class OfertaConverter extends AbstractConverter<Oferta, OfertaDTO> {

    public OfertaConverter() {
        //constructor
    }

    @Override
    public Oferta fromDto(OfertaDTO dto) {
        Oferta oferta = new Oferta();
        oferta.setDescripcon(dto.getDescripcon());
        oferta.setEstado(dto.getEstado());
        oferta.setFecha(dto.getFecha());
        oferta.setFkIdUsuario(dto.getFkIdUsuario());
        oferta.setUbicacion(dto.getUbicacion());
        oferta.setValorDia(dto.getValorDia());

        return oferta;
    }

    @Override
    public OfertaDTO fromEntity(Oferta entity) {
        OfertaDTO ofertaDTO = new OfertaDTO();
        ofertaDTO.setDescripcon(entity.getDescripcon());
        ofertaDTO.setEstado(entity.getEstado());
        ofertaDTO.setFecha(entity.getFecha());
        ofertaDTO.setFkIdUsuario(entity.getFkIdUsuario());
        ofertaDTO.setUbicacion(entity.getUbicacion());
        ofertaDTO.setValorDia(entity.getValorDia());

        return ofertaDTO;
    }
}
