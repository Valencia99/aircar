package com.proyecto.AirCar.repository;

import com.proyecto.AirCar.model.Oferta;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OfertaRepository extends JpaRepository<Oferta, Long> {

    @Query(value = "Select * from oferta where fk_id_usuario = :idUsuario", nativeQuery = true)
    public List<Oferta> findOfertaByFkIdUsuario(Long idUsuario);

    @Query(value = "Select * from oferta where fk_id_vehiculo = :idVehiculo", nativeQuery = true)
    public List<Oferta> findOfertaByFkIdVehiculo(Long idVehiculo);
}
