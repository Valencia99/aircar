package com.proyecto.AirCar.controller;

import com.proyecto.AirCar.converter.VehiculoConverter;
import com.proyecto.AirCar.dto.VehiculoDTO;
import com.proyecto.AirCar.dto.VehiculoOfertaDTO;
import com.proyecto.AirCar.exception.VehiculoRequestException;
import com.proyecto.AirCar.model.Vehiculo;
import com.proyecto.AirCar.service.VehiculoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/vehiculo")
@Api("vehiculo")
public class VehiculoController {

    private final VehiculoService vehiculoService;

    @Autowired
    public VehiculoController(VehiculoService vehiculoService) {
        this.vehiculoService = vehiculoService;
    }

    @PostMapping(path = "/save")
    @ApiOperation(value = "Save vehiculo", response = Vehiculo.class)
    public Vehiculo saveVehiculo(@RequestBody VehiculoDTO vehiculoDTO) {
        VehiculoConverter vehiculoConverter = new VehiculoConverter();
        Vehiculo vehiculo = vehiculoConverter.fromDto(vehiculoDTO);
        return vehiculoService.saveVehiculo(vehiculo);
    }

    @PutMapping(path = "/update")
    @ApiOperation(value = "Update vehiculo", response = Vehiculo.class)
    public Vehiculo updateVehiculo(@RequestBody VehiculoDTO vehiculoDTO) {
        VehiculoConverter vehiculoConverter = new VehiculoConverter();
        Vehiculo vehiculo = vehiculoConverter.fromDto(vehiculoDTO);
        return vehiculoService.updateVehiculo(vehiculo);
    }

    @GetMapping(path = "/all")
    @ApiOperation(value = "Get vehiculo", response = Vehiculo.class)
    public List<Vehiculo> findAll() {
        return vehiculoService.findAll();
    }

    @DeleteMapping(path = "/delete")
    @ApiOperation(value = "Delete vehiculo", response = Vehiculo.class)
    public void deleteVehiculo(@RequestParam(name = "idVehiculo") Long idVehiculo) {
        vehiculoService.deleteVehiculo(idVehiculo);
    }

    @GetMapping(path = "/vehiculo/info")
    @ApiOperation(value = "buscar vehiculo y oferta", response = Vehiculo.class)
    public ResponseEntity<VehiculoOfertaDTO> findbyIdVehOfer(@RequestParam(name = "idVehiculo") long idVehiculo, @RequestParam(name = "idOferta") Long idOferta) {
        VehiculoRequestException vehException = new VehiculoRequestException("no se encuentra la informacion del vehiculo con la oferta");
        return ResponseEntity.ok(vehiculoService.findbyIdVehOfer(idVehiculo, idOferta)
                .orElseThrow(() -> vehException));
    }

}
