package com.proyecto.AirCar.controller;

import com.proyecto.AirCar.converter.OfertaConverter;
import com.proyecto.AirCar.dto.OfertaDTO;
import com.proyecto.AirCar.model.Oferta;
import com.proyecto.AirCar.service.OfertaService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/oferta")
@Api("ofrta")
public class OfertaController {

    private final OfertaService ofertaService;

    @Autowired
    public OfertaController(OfertaService ofertaService) {
        this.ofertaService = ofertaService;
    }

    @PostMapping(path = "/save")
    @ApiOperation(value = "Save oferta", response = Oferta.class)
    public Oferta saveOferta(@Valid @RequestBody OfertaDTO ofertaDTO) {
        OfertaConverter ofertaConverter = new OfertaConverter();
        Oferta oferta = ofertaConverter.fromDto(ofertaDTO);
        return ofertaService.saveOferta(oferta);
    }

    @PutMapping(path = "/update")
    @ApiOperation(value = "Update oferta", response = Oferta.class)
    public Oferta updateOferta(@Valid @RequestBody OfertaDTO ofertaDTO) {
        OfertaConverter ofertaConverter = new OfertaConverter();
        Oferta oferta = ofertaConverter.fromDto(ofertaDTO);
        return ofertaService.updateOferta(oferta);
    }

    @GetMapping(path = "/all")
    @ApiOperation(value = "Get oferta", response = Oferta.class)
    public List<Oferta> findAll() {
        return ofertaService.findAll();
    }

    @DeleteMapping(path = "/delete")
    @ApiOperation(value = "Delete oferta", response = Oferta.class)
    public void deleteOferta(@RequestParam(name = "idOferta") Long idOferta) {
        ofertaService.deleteOferta(idOferta);
    }

    
}
