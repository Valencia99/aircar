package com.proyecto.AirCar.controller;

import com.proyecto.AirCar.converter.ReservacionConverter;
import com.proyecto.AirCar.dto.ReservacionDTO;
import com.proyecto.AirCar.model.Reservacion;
import com.proyecto.AirCar.service.ReservacionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/reservacion")
@Api("reservacion")
public class ReservacionController {

    private final ReservacionService reservacionService;

    @Autowired
    public ReservacionController(ReservacionService reservacionService) {
        this.reservacionService = reservacionService;
    }

    @PostMapping(path = "/save")
    @ApiOperation(value = "Save reserva", response = Reservacion.class)
    public Reservacion saveReserva(@Valid @RequestBody ReservacionDTO reservacionDTO) {
        ReservacionConverter reservacionConverter = new ReservacionConverter();
        Reservacion reservacion = reservacionConverter.fromDto(reservacionDTO);
        return reservacionService.saveReserva(reservacion);
    }

    @PutMapping(path = "/update")
    @ApiOperation(value = "Update resera", response = Reservacion.class)
    public Reservacion updateReserva(@RequestBody ReservacionDTO reservacionDTO) {
        ReservacionConverter reservacionConverter = new ReservacionConverter();
        Reservacion reservacion = reservacionConverter.fromDto(reservacionDTO);
        return reservacionService.updateReserva(reservacion);
    }

    @GetMapping(path = "/all")
    @ApiOperation(value = "Get reserva", response = Reservacion.class)
    public List<Reservacion> findAll() {
        return reservacionService.findAll();
    }

    @DeleteMapping(path = "/delete")
    @ApiOperation(value = "Delete reserva", response = Reservacion.class)
    public void deleteReserva(@RequestParam(name = "id_reservacion") Long idReserva) {
        reservacionService.deleteReserva(idReserva);
    }

}
