package com.proyecto.AirCar.controller;

import com.proyecto.AirCar.converter.UsuarioConverter;
import com.proyecto.AirCar.dto.UsuarioDTO;
import com.proyecto.AirCar.model.Usuario;
import com.proyecto.AirCar.service.UsuarioService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/usuario")
@Api("usuario")
public class UsuarioController {

    private final UsuarioService usuarioService;

    @Autowired
    public UsuarioController(UsuarioService usuarioService) {
        this.usuarioService = usuarioService;
    }


    @PostMapping(path = "/save")
    @ApiOperation(value = "Save usuario", response = Usuario.class)
    public Usuario saveUsuario(@RequestBody UsuarioDTO usuarioDTO){
        //usuario.setClave(bCryptPasswordEncoder.encode(usuario.getClave()));
        UsuarioConverter usuarioConverter = new UsuarioConverter();
        Usuario usuario = usuarioConverter.fromDto(usuarioDTO);
        return usuarioService.saveUsuario(usuario);
    }


}
