package com.proyecto.AirCar.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "reservacion")
public class Reservacion {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_reservacion")
    private Long idReserva;

    @Column(name = "fecha_entrega")
    private Date fechaEntrega;

    @Column(name = "fecha_devolucion")
    private Date fechaDevolucion;

    @Column(name = "valor_seguro")
    private Integer valorSeguro;

    @Column(name = "valor_total")
    private Integer valorTotal;

    @Column(name = "fk_id_oferta")
    private Integer fkIdOferta;

    @Column(name = "numero_licencia")
    private Integer numeroLicencia;

    @Column(name = "estado")
    private Boolean estado;

    public Reservacion() {
    }

    public Reservacion(Long idReserva, Date fechaEntrega, Date fechaDevolucion, Integer valorSeguro, Integer valorTotal, Integer fkIdOferta, Integer numeroLicencia, Boolean estado) {
        this.idReserva = idReserva;
        this.fechaEntrega = fechaEntrega;
        this.fechaDevolucion = fechaDevolucion;
        this.valorSeguro = valorSeguro;
        this.valorTotal = valorTotal;
        this.fkIdOferta = fkIdOferta;
        this.numeroLicencia = numeroLicencia;
        this.estado = estado;
    }

    public Long getIdReserva() {
        return idReserva;
    }

    public void setIdReserva(Long idReserva) {
        this.idReserva = idReserva;
    }

    public Date getFechaEntrega() {
        return fechaEntrega;
    }

    public void setFechaEntrega(Date fechaEntrega) {
        this.fechaEntrega = fechaEntrega;
    }

    public Date getFechaDevolucion() {
        return fechaDevolucion;
    }

    public void setFechaDevolucion(Date fechaDevolucion) {
        this.fechaDevolucion = fechaDevolucion;
    }

    public Integer getValorSeguro() {
        return valorSeguro;
    }

    public void setValorSeguro(Integer valorSeguro) {
        this.valorSeguro = valorSeguro;
    }

    public Integer getValorTotal() {
        return valorTotal;
    }

    public void setValorTotal(Integer valorTotal) {
        this.valorTotal = valorTotal;
    }

    public Integer getFkIdOferta() {
        return fkIdOferta;
    }

    public void setFkIdOferta(Integer fkIdOferta) {
        this.fkIdOferta = fkIdOferta;
    }

    public Integer getNumeroLicencia() {
        return numeroLicencia;
    }

    public void setNumeroLicencia(Integer numeroLicencia) {
        this.numeroLicencia = numeroLicencia;
    }

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }
}

