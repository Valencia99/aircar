package com.proyecto.AirCar.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "vehiculo")
public class Vehiculo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_vehiculo")
    private Long idVehiculo;

    @Column(name = "marca")
    private String marca;

    @Column(name = "modelo")
    private String modelo;

    @Column(name = "aniu")
    private Integer aniu;

    @Column(name = "color")
    private String color;

    @Column(name = "placa")
    private String placa;

    @Column(name = "descripcion")
    private String descripcion;

    @Column(name = "estado_vehiculo")
    private Boolean estadoVehiculo;

    @Column(name = "fk_usuario")
    private Integer usuarioVehiculo;

    public Vehiculo() {
    }

    public Vehiculo(Long idVehiculo, String marca, String modelo, Integer aniu, String color, String placa, String descripcion, Integer usuarioVehiculo, Boolean estadoVehiculo) {
        this.idVehiculo = idVehiculo;
        this.marca = marca;
        this.modelo = modelo;
        this.aniu = aniu;
        this.color = color;
        this.placa = placa;
        this.descripcion = descripcion;
        this.usuarioVehiculo = usuarioVehiculo;
        this.estadoVehiculo = estadoVehiculo;
    }

    public Integer getUsuarioVehiculo() {
        return usuarioVehiculo;
    }

    public void setUsuarioVehiculo(Integer usuarioVehiculo) {
        this.usuarioVehiculo = usuarioVehiculo;
    }

    public Long getIdVehiculo() {
        return idVehiculo;
    }

    public void setIdVehiculo(Long idVehiculo) {
        this.idVehiculo = idVehiculo;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public Integer getAniu() {
        return aniu;
    }

    public void setAniu(Integer aniu) {
        this.aniu = aniu;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Boolean getEstadoVehiculo() {
        return estadoVehiculo;
    }

    public void setEstadoVehiculo(Boolean estadoVehiculo) {
        this.estadoVehiculo = estadoVehiculo;
    }
}
