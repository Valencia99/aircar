package com.proyecto.AirCar.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "oferta")
public class Oferta {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_oferta")
    private Long idOferta;

    @ManyToOne
    @JoinColumn(name = "fk_id_vehiculo")
    private Oferta oferta;

    @Column(name = "fk_id_usuario")
    private Long fkIdUsuario;

    @Column(name = "fecha")
    private Date fecha;

    @Column(name = "descripcion")
    private String descripcon;

    @Column(name = "valor_dia")
    private Integer valorDia;

    @Column(name = "ubicacion")
    private String ubicacion;

    @Column(name = "estado")
    private Boolean estado;


    public Oferta() {
        //constructor
    }

    public Oferta(Long idOferta, Long fkIdUsuario, Date fecha, String descripcon, Integer valorDia, String ubicacion, Boolean estado) {
        this.idOferta = idOferta;
        this.fkIdUsuario = fkIdUsuario;
        this.fecha = fecha;
        this.descripcon = descripcon;
        this.valorDia = valorDia;
        this.ubicacion = ubicacion;
        this.estado = estado;
    }

    public Long getIdOferta() {
        return idOferta;
    }

    public void setIdOferta(Long idOferta) {
        this.idOferta = idOferta;
    }

    public Long getFkIdUsuario() {
        return fkIdUsuario;
    }

    public void setFkIdUsuario(Long fkIdUsuario) {
        this.fkIdUsuario = fkIdUsuario;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getDescripcon() {
        return descripcon;
    }

    public void setDescripcon(String descripcon) {
        this.descripcon = descripcon;
    }

    public Integer getValorDia() {
        return valorDia;
    }

    public void setValorDia(Integer valorDia) {
        this.valorDia = valorDia;
    }

    public String getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }
}
