package com.proyecto.AirCar.service;

import com.proyecto.AirCar.model.Oferta;
import com.proyecto.AirCar.repository.OfertaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OfertaService {

    private final OfertaRepository ofertaRepository;

    @Autowired
    public OfertaService(OfertaRepository ofertaRepository) {
        this.ofertaRepository = ofertaRepository;
    }

    public Oferta saveOferta(Oferta oferta) {
        return ofertaRepository.save(oferta);
    }

    public void deleteOferta(Long idOferta) {
        ofertaRepository.deleteById(idOferta);
    }

    public Oferta updateOferta(Oferta oferta) {
        return ofertaRepository.save(oferta);
    }

    public List<Oferta> findAll() {
        return ofertaRepository.findAll();
    }

    public List<Oferta> findOfertaByFkIdUsuario(Long idUsuario){return ofertaRepository.findOfertaByFkIdUsuario(idUsuario);}

    public List<Oferta> findOfertaByFkIdVehiculo(Long idVehiculo){return ofertaRepository.findOfertaByFkIdVehiculo(idVehiculo);}
}
