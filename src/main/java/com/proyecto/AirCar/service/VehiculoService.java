package com.proyecto.AirCar.service;

import com.proyecto.AirCar.dto.VehiculoOfertaDTO;
import com.proyecto.AirCar.model.Oferta;
import com.proyecto.AirCar.model.Vehiculo;
import com.proyecto.AirCar.repository.OfertaRepository;
import com.proyecto.AirCar.repository.VehiculoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class VehiculoService {

    private final VehiculoRepository vehiculoRepository;
    private final OfertaRepository ofertaRepository;

    @Autowired
    public VehiculoService(VehiculoRepository vehiculoRepository, OfertaRepository ofertaRepository) {
        this.vehiculoRepository = vehiculoRepository;
        this.ofertaRepository = ofertaRepository;
    }

    public Vehiculo saveVehiculo(Vehiculo vehiculo) {
        return vehiculoRepository.save(vehiculo);
    }

    public void deleteVehiculo(Long idVehiculo) {
        vehiculoRepository.deleteById(idVehiculo);
    }

    public Vehiculo updateVehiculo(Vehiculo vehiculo) {
        return vehiculoRepository.save(vehiculo);
    }

    public List<Vehiculo> findAll() {
        return vehiculoRepository.findAll();
    }

    public Optional<VehiculoOfertaDTO> findbyIdVehOfer(Long idVehiculo, Long idOferta) {
        VehiculoOfertaDTO vehDTO = new VehiculoOfertaDTO();
        Optional<Vehiculo> veh = vehiculoRepository.findById(idVehiculo);
        Optional<Oferta> ofer = ofertaRepository.findById(idOferta);
        if (veh.isPresent() && ofer.isPresent()) {
            vehDTO.setColorVehiculo(veh.get().getColor());
            vehDTO.setMarcaVehiculo(veh.get().getMarca());
            vehDTO.setModeloVehiculo(veh.get().getModelo());
            vehDTO.setAniuVehiculo(veh.get().getAniu());
            vehDTO.setPlacaVehiculo(veh.get().getPlaca());
            vehDTO.setDescripconOferta(ofer.get().getDescripcon());
            vehDTO.setFechaOferta(ofer.get().getFecha());
            vehDTO.setUbicacionOferta(ofer.get().getUbicacion());
            vehDTO.setValorDiaOferta(ofer.get().getValorDia());
        }
        return Optional.of(vehDTO);
    }

}
