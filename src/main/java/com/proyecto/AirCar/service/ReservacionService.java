package com.proyecto.AirCar.service;

import com.proyecto.AirCar.model.Reservacion;
import com.proyecto.AirCar.repository.ReservacionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ReservacionService {

    private final ReservacionRepository reservaRepository;


    @Autowired
    public ReservacionService(ReservacionRepository reservaRepository) {
        this.reservaRepository = reservaRepository;

    }

    public Reservacion saveReserva(Reservacion reserva) {
        return reservaRepository.save(reserva);
    }

    public void deleteReserva(Long idReserva) {
        reservaRepository.deleteById(idReserva);
    }

    public Reservacion updateReserva(Reservacion reserva) {
        return reservaRepository.save(reserva);
    }

    public List<Reservacion> findAll() {
        return reservaRepository.findAll();
    }

}
