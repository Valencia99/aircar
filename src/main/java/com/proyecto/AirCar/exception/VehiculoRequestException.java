package com.proyecto.AirCar.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class VehiculoRequestException extends RuntimeException {

    public VehiculoRequestException(String message) {
        super(message);
    }

    public VehiculoRequestException(String message, Throwable cause) {
        super(message, cause);
    }

}
